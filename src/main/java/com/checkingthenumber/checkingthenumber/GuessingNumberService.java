package com.checkingthenumber.checkingthenumber;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Log4j2
public class GuessingNumberService {

    private static final String TRIGGER_NAME = "start";

    private final WebClient webClient;
    private final Producer producer;

    @Value("${number-range.min}")
    private int min;
    @Value("${number-range.max}")
    private int max;
    @Value("${security.tokens}")
    private String token;
    @Value("${api.baseUrl}")
    private String baseUrl;

    public void guessNumber(String message) {

        if (message.equals(TRIGGER_NAME)) {
            int minValue = min;
            int maxValue = max;
            int value = maxValue - ((maxValue - minValue) / 2);
            int counter = 1;
            Integer checkingNumber = callExternalApi(value);
            while (checkingNumber != 0) {
                if (checkingNumber == -1) {
                    minValue = value;
                } else {
                    maxValue = value;
                }
                value = maxValue - ((maxValue - minValue) / 2);
                checkingNumber = callExternalApi(value);
                counter++;
            }
            producer.sendToQueue(counter);
            log.info("Number is: " + value);
            log.info("Count: " + counter);
        } else {
            throw new ApiException("Wrong message to trigger the process.");
        }
    }

    private Integer callExternalApi(final Integer numberToCheck) {
        Mono<Integer> response = webClient.get()
                .uri(baseUrl + numberToCheck)
                .header("Authorization", token)
                .retrieve()
                .bodyToMono(Integer.class);
        return Optional.ofNullable(response.block()).orElseThrow(() -> new ApiException("Could not connect to the service!"));
    }
}
