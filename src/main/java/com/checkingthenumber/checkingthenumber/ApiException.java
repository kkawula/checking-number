package com.checkingthenumber.checkingthenumber;

public class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }
}
