package com.checkingthenumber.checkingthenumber;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
public enum QueueNames {
    STARTS(Constants.STARTS_VALUE),
    END("END"),
    ;

    private final String value;

    public String getValue() {
        return value;
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Constants {
        public static final String STARTS_VALUE = "STARTS";
    }
}
