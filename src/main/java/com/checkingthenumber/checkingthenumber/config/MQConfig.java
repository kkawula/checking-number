package com.checkingthenumber.checkingthenumber.config;

import com.checkingthenumber.checkingthenumber.QueueNames;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MQConfig {

    @Bean
    Queue queue() {
        return new Queue(QueueNames.STARTS.name(), false);
    }

    @Bean
    Queue queueEnd() {
        return new Queue(QueueNames.END.name(), false);
    }
}
