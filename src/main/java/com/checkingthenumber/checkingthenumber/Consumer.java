package com.checkingthenumber.checkingthenumber;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
@RequiredArgsConstructor
public class Consumer {

    private final GuessingNumberService guessingNumberService;

    @RabbitListener(queues = QueueNames.Constants.STARTS_VALUE)
    public void listeningFromQueue(String message) {
        try {
            guessingNumberService.guessNumber(message);
        }catch (Exception e) {
            log.error("Invalid message!", e);
            throw new AmqpRejectAndDontRequeueException("Invalid message!");
        }
    }
}
