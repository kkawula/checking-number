package com.checkingthenumber.checkingthenumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckingTheNumberApplication {

    public static void main(String[] args) {
        SpringApplication.run(CheckingTheNumberApplication.class, args);
    }

}
