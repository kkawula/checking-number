package com.checkingthenumber.checkingthenumber;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Producer {

    private final AmqpTemplate amqpTemplate;

    public void sendToQueue(final Integer tries) {
        String numberOfTries = "Guessed number after " + tries + " tries";
        amqpTemplate.convertAndSend(QueueNames.END.getValue(), numberOfTries);
    }
}
